<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        DB::table('plans')->insert([
            'type' => 'ONETIME',
            'name' => 'Pro',
            'price' => 20,
            'trial_days' => '3',
            'on_install' => true,
        ]);
    }
}
